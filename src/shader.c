#include "internal.h"
#include "glsw.h"
#include <string.h>
#include <stdio.h>

static
GLuint
compile_shader(GLenum type, const char* source) {
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar * const*)&source, NULL);
    glCompileShader(shader);

    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE) {
        printf("ERROR: FAILED TO COMPILE SHADER!\n");
    }

    GLint logLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 1) {
        char * log = malloc((logLength)*sizeof(char));
        glGetShaderInfoLog(shader, logLength, NULL, log);
        printf("Shader error:\n%s\nSource:\n%s", log, source);
        free(log);
        return -1;
    }
    return shader;
}

L3D_EXPORTED
struct l3d_shader*
l3d_shader_load(const char* key, const char* variant, struct l3d_shader_config const* conf) {
    char k[1024];
    if (variant) sprintf(k, "%s.Vertex.%s", key, variant);
    else sprintf(k, "%s.Vertex", key);
    const char* vertex_shader_source = glswGetShader(k);
    assert(vertex_shader_source);

    if (variant) sprintf(k, "%s.Fragment.%s", key, variant);
    else sprintf(k, "%s.Fragment", key);
    const char* fragment_shader_source = glswGetShader(k);
    assert(fragment_shader_source);

    if (!vertex_shader_source || !fragment_shader_source) {
        printf("Failed to load shader: %s\n", key);
        return NULL;
    }

    int32_t id = glCreateProgram();
    glAttachShader(id, compile_shader(GL_VERTEX_SHADER, vertex_shader_source));
    glAttachShader(id, compile_shader(GL_FRAGMENT_SHADER, fragment_shader_source));

#ifdef GLMAX
    if (variant) sprintf(k, "%s.TessControl.%s", key, variant);
    else sprintf(k, "%s.TessControl", key);
    const char* tc = glswGetShader(k);
    if (tc) {
        glPatchParameteri(GL_PATCH_VERTICES, 3);
        glAttachShader(id, compile_shader(GL_TESS_CONTROL_SHADER, tc));
    }

    if (variant) sprintf(k, "%s.TessEvaluation.%s", key, variant);
    else sprintf(k, "%s.TessEvaluation", key);
    const char* te = glswGetShader(k);
    if (te) {
        glAttachShader(id, compile_shader(GL_TESS_EVALUATION_SHADER, te));
    }
#endif

    if (variant) sprintf(k, "%s.Geometry.%s", key, variant);
    else sprintf(k, "%s.Geometry", key);
    const char* ge = glswGetShader(k);
    if (ge) {
        glAttachShader(id, compile_shader(GL_GEOMETRY_SHADER, ge));
    }

    glLinkProgram(id);
    glUseProgram(id);

    GLint logLength;
    glGetProgramiv(id, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 1) {
        char* log = malloc((logLength)*sizeof(char));
        glGetProgramInfoLog(id, logLength, NULL, log);
        printf("Linker message:\n%s", log);
        free(log);
        //return NULL;
    }

    GLint linkStatus = 0;
    glGetProgramiv(id, GL_LINK_STATUS, &linkStatus);
    if(GL_FALSE == linkStatus) {
        printf("ERROR: FAILED TO LINK PROGRAM!\n");
        return NULL;
    }

    GLenum error = glGetError();
    if(GL_NO_ERROR != error) {
        printf("GL ERROR: %d\n", error);
    }


    struct l3d_shader* s = malloc(sizeof(struct l3d_shader));
#ifdef GLMAX
    s->draw_patches = (tc != NULL);
#else
    s->draw_patches = false;
#endif
    s->id = id;
    s->a_position = glGetAttribLocation(id, "position");
    s->a_normal = glGetAttribLocation(id, "normal");
    s->a_tangent = glGetAttribLocation(id, "tangent");
    s->a_uv = glGetAttribLocation(id, "uv");
    s->a_bone_ids = glGetAttribLocation(id, "bone_ids");
    s->a_bone_weights = glGetAttribLocation(id, "bone_weights");

    s->u_pv = glGetUniformLocation(id, "pv");
    s->u_time = glGetUniformLocation(id, "time");
    s->u_camera = glGetUniformLocation(id, "camera");
    s->u_pixel_size = glGetUniformLocation(id, "pixel_size");
    s->u_color = glGetUniformLocation(id, "color");
    s->u_model_matrix = glGetUniformLocation(id, "model_matrix");
    s->u_view_matrix = glGetUniformLocation(id, "view_matrix");
    s->u_projection_matrix = glGetUniformLocation(id, "projection_matrix");
    s->u_pv_inverse = glGetUniformLocation(id, "pv_inverse");
    s->u_tess_level = glGetUniformLocation(id, "tess_level");
    s->u_bones = glGetUniformLocation(id, "bones");

    s->texture_slots = NULL;
    for (int i=0; i<32; i++) {
        if (conf->texture_slots[i] == NULL)
            break;
        int texid = glGetUniformLocation(id, conf->texture_slots[i]);
        if (texid == -1) {
            printf("Error finding texture uniform \"%s\" in shader \"%s\"\n", conf->texture_slots[i], key);
        }
        sbpush(s->texture_slots, texid);
    }

    return s;
}
