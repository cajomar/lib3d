#include "internal.h"

struct face {
    float normal[3];
    float position;
    float l, t, r, b;
};

static
inline
float
dot(float* a, float* b) {
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

void
i_calc_tangents(struct l3d_vertex* vs, int count) {
    for (int i=0; i<count; i+=3) {
        struct l3d_vertex v1 = vs[i];
        struct l3d_vertex v2 = vs[i+1];
        struct l3d_vertex v3 = vs[i+2];

        float x1 = v2.position[0] - v1.position[0];
        float x2 = v3.position[0] - v1.position[0];
        float y1 = v2.position[1] - v1.position[1];
        float y2 = v3.position[1] - v1.position[1];
        float z1 = v2.position[2] - v1.position[2];
        float z2 = v3.position[2] - v1.position[2];
        
        float s1 = v2.u - v1.u;
        float s2 = v3.u - v1.u;
        float t1 = v2.v - v1.v;
        float t2 = v3.v - v1.v;

        float r = 1.0F / (s1 * t2 - s2 * t1);
        vec3 tan1;
        tan1[0] = (t2 * x1 - t1 * x2) * r;
        tan1[1] = (t2 * y1 - t1 * y2) * r;
        tan1[2] = (t2 * z1 - t1 * z2) * r;
        vec3 tan2;
        tan2[0] = (s1 * x2 - s2 * x1) * r;
        tan2[1] = (s1 * y2 - s2 * y1) * r;
        tan2[2] = (s1 * z2 - s2 * z1) * r;

        for (int j=0; j<3; j++) {
            vec3 n = {vs[i+j].normal[0], vs[i+j].normal[1], vs[i+j].normal[2]};

            // normalized(tan1 - n * dot(n, tan1))
            vec3 scaled;
            vec3_scale(scaled, n, dot(n, tan1));
            vec3 subbed;
            vec3_sub(subbed, tan1, scaled);
            vec3 tangent;
            vec3_norm(tangent, subbed);

            vs[i+j].tangent_normal[0] = tangent[0];
            vs[i+j].tangent_normal[1] = tangent[1];
            vs[i+j].tangent_normal[2] = tangent[2];

            vec3 crossed;
            vec3_mul_cross(crossed, n, tan1);
            if (dot(crossed, tan2) < 0) {
                vs[i+j].tangent_normal[0] *= -1;
                vs[i+j].tangent_normal[1] *= -1;
                vs[i+j].tangent_normal[2] *= -1;
            }
        }
    }
}

static
void
gen_geom_square(struct l3d_vertex* out, int* index,
        float x_tl, float y_tl,  float u_tl, float v_tl,
        float x_tr, float y_tr,  float u_tr, float v_tr,
        float x_br, float y_br,  float u_br, float v_br,
        float x_bl, float y_bl,  float u_bl, float v_bl,
        float z_t, float z_b,
        float nx, float ny, float nz,
        float tx, float ty, float tz) {
    x_tl += tx; x_tr += tx; x_br += tx; x_bl += tx;
    y_tl += ty; y_tr += ty; y_br += ty; y_bl += ty;
    z_t += tz; z_b += tz;

    int i = *index;
    out[i].position[0] = x_tl; out[i].position[1] = y_tl; out[i].position[2] = z_t; out[i].position[3] = 1;
    out[i].u = u_tl; out[i].v = v_tl;
    out[i].normal[0] = nx; out[i].normal[1] = ny; out[i].normal[2] = nz;
    i++;

    out[i].position[0] = x_br; out[i].position[1] = y_br; out[i].position[2] = z_b; out[i].position[3] = 1;
    out[i].u = u_br; out[i].v = v_br;
    out[i].normal[0] = nx; out[i].normal[1] = ny; out[i].normal[2] = nz;
    i++;

    out[i].position[0] = x_bl; out[i].position[1] = y_bl; out[i].position[2] = z_b; out[i].position[3] = 1;
    out[i].u = u_bl; out[i].v = v_bl;
    out[i].normal[0] = nx; out[i].normal[1] = ny; out[i].normal[2] = nz;
    i++;


    out[i].position[0] = x_br; out[i].position[1] = y_br; out[i].position[2] = z_b; out[i].position[3] = 1;
    out[i].u = u_br; out[i].v = v_br;
    out[i].normal[0] = nx; out[i].normal[1] = ny; out[i].normal[2] = nz;
    i++;

    out[i].position[0] = x_tl; out[i].position[1] = y_tl; out[i].position[2] = z_t; out[i].position[3] = 1;
    out[i].u = u_tl; out[i].v = v_tl;
    out[i].normal[0] = nx; out[i].normal[1] = ny; out[i].normal[2] = nz;
    i++;

    out[i].position[0] = x_tr; out[i].position[1] = y_tr; out[i].position[2] = z_t; out[i].position[3] = 1;
    out[i].u = u_tr; out[i].v = v_tr;
    out[i].normal[0] = nx; out[i].normal[1] = ny; out[i].normal[2] = nz;
    i++;

    *index = i;
}

L3D_EXPORTED
void
l3d_geom_write_square(struct l3d_vertex* target, vec3 tl, vec3 tr, vec3 br, vec3 bl,
        vec2 uv[4]) {
    int i = 0;

    float u_tl = 0, v_tl = 0;
    float u_tr = 1, v_tr = 0;
    float u_br = 1, v_br = 1;
    float u_bl = 0, v_bl = 1;

    if (uv) {
        u_tl = uv[0][0]; v_tl = uv[0][1];
        u_tr = uv[1][0]; v_tr = uv[1][1];
        u_br = uv[2][0]; v_br = uv[2][1];
        u_bl = uv[3][0]; v_bl = uv[3][1];
    }

    vec3 n, u, v;
    float *p1, *p2, *p3;

    p1 = tl; p2 = br; p3 = bl;
    vec4_sub(u, p2, p1);
    vec4_sub(v, p3, p1);
    n[0] = u[1]*v[2] - u[2]*v[1];
    n[1] = u[2]*v[0] - u[0]*v[2];
    n[2] = u[0]*v[1] - u[1]*v[0];
    vec3_norm(n, n);

    memcpy(target[i].position, tl, sizeof(vec3));
    target[i].u = u_tl; target[i].v = v_tl;
    memcpy(target[i].normal, n, sizeof(vec3));
    i++;

    memcpy(target[i].position, br, sizeof(vec3));
    target[i].u = u_br; target[i].v = v_br;
    memcpy(target[i].normal, n, sizeof(vec3));
    i++;

    memcpy(target[i].position, bl, sizeof(vec3));
    target[i].u = u_bl; target[i].v = v_bl;
    memcpy(target[i].normal, n, sizeof(vec3));
    i++;


    p1 = br; p2 = tl; p3 = tr;
    vec4_sub(u, p2, p1);
    vec4_sub(v, p3, p1);
    n[0] = u[1]*v[2] - u[2]*v[1];
    n[1] = u[2]*v[0] - u[0]*v[2];
    n[2] = u[0]*v[1] - u[1]*v[0];
    vec3_norm(n, n);

    memcpy(target[i].position, br, sizeof(vec3));
    target[i].u = u_br; target[i].v = v_br;
    memcpy(target[i].normal, n, sizeof(vec3));
    i++;

    memcpy(target[i].position, tl, sizeof(vec3));
    target[i].u = u_tl; target[i].v = v_tl;
    memcpy(target[i].normal, n, sizeof(vec3));
    i++;

    memcpy(target[i].position, tr, sizeof(vec3));
    target[i].u = u_tr; target[i].v = v_tr;
    memcpy(target[i].normal, n, sizeof(vec3));
    i++;

    i_calc_tangents(target, 6);
}

static
void
gen_geom_cube(struct face* face,
        struct l3d_vertex* out, int* i,
        float sx, float sy, float sz,
        vec2 uv[4],
        float tx, float ty, float tz) {

    float u_tl = 0, v_tl = 0;
    float u_tr = 1, v_tr = 0;
    float u_br = 1, v_br = 1;
    float u_bl = 0, v_bl = 1;

    if (uv) {
        u_tl = uv[0][0]; v_tl = uv[0][1];
        u_tr = uv[1][0]; v_tr = uv[1][1];
        u_br = uv[2][0]; v_br = uv[2][1];
        u_bl = uv[3][0]; v_bl = uv[3][1];
    }

    // top face
    gen_geom_square(out, i,
        -sx, -sy,  u_tr, v_tr,
        sx, -sy,   u_tl, v_tl,
        sx, sy,    u_bl, v_bl,
        -sx, sy,   u_br, v_br,
        sz, sz,
        0.0, 0.0, 1.0,
        tx,ty,tz);

    // bottom face
    gen_geom_square(out, i,
        -sx, sy,  u_tr, v_tr,
        sx, sy,   u_tl, v_tl,
        sx, -sy,  u_bl, v_bl,
        -sx, -sy, u_br, v_br,
        -sz, -sz,
        0.0, 0.0, -1.0,
        tx,ty,tz);

    // back face
    gen_geom_square(out, i,
        sx, -sy,   u_tr, v_tr,
        -sx, -sy,  u_tl, v_tl,
        -sx, -sy,  u_bl, v_bl,
        sx, -sy,   u_br, v_br,
        sz, -sz,
        0.0, -1.0, 0.0,
        tx,ty,tz);
    if (face) {
        face->normal[0] = 0;
        face->normal[1] = -1;
        face->normal[2] = 0;
        face->position = sy + ty;
        face->l = -sx + tx;
        face->r = sx + tx;
        face->t = sz + tz;
        face->b = tz;
    }

    // front face
    gen_geom_square(out, i,
        -sx, sy,  u_tr, v_tr,
        sx, sy,   u_tl, v_tl,
        sx, sy,   u_bl, v_bl,
        -sx, sy,  u_br, v_br,
        sz, -sz,
        0.0, 1.0, 0.0,
        tx,ty,tz);

    // right edge face
    gen_geom_square(out, i,
        sx, sy,   u_tr, v_tr,
        sx, -sy,  u_tl, v_tl,
        sx, -sy,  u_bl, v_bl,
        sx, sy,   u_br, v_br,
        sz, -sz,
        1.0, 0.0, 0.0,
        tx,ty,tz);

    // left edge face
    gen_geom_square(out, i,
        -sx, -sy,  u_tr, v_tr,
        -sx, sy,   u_tl, v_tl,
        -sx, sy,   u_bl, v_bl,
        -sx, -sy,  u_br, v_br,
        sz, -sz,
        -1.0, 0.0, 0.0,
        tx,ty,tz);
}

L3D_EXPORTED
struct l3d_mesh*
l3d_geom_plane(float sx, float sy) {
    struct l3d_vertex* out = malloc(sizeof(struct l3d_vertex) * 6);
    int i = 0;

    float uw = 1;
    float vh = 1;
    gen_geom_square(out, &i,
        -sx, -sy,  0,  0,
        sx, -sy,   uw, 0,
        sx, sy,    uw, vh,
        -sx, sy,   0,  vh,
        0, 0,
        0.0, 0.0, 1.0,
        0,0,0);

    i_calc_tangents(out, 6);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(struct l3d_vertex)*i, out, GL_STATIC_DRAW);
    struct l3d_mesh* m = malloc(sizeof(struct l3d_mesh));
    m->bones = 0;
    m->bone_names = 0;
    m->indexed = false;
    m->indicies = 0;
    m->vbo = vbo;
    m->verts = out;
    m->vertex_count = i;
    m->bsphere.center[0] = 0;
    m->bsphere.center[1] = 0;
    m->bsphere.center[2] = 0;
    m->bsphere.rad = sx > sy ? sx : sy;
    return m;
}

L3D_EXPORTED
struct l3d_mesh*
l3d_geom_cube(float sx, float sy, float sz, vec2 uv[4]) {
    struct l3d_vertex* out = malloc(sizeof(struct l3d_vertex) * 6*6);
    int i = 0;

    gen_geom_cube(NULL,
        out, &i,
        sx,
        sy,
        sz,
        uv,
        0,0,0);

    i_calc_tangents(out, 6*6);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(struct l3d_vertex)*i, out, GL_STATIC_DRAW);
    struct l3d_mesh* m = malloc(sizeof(struct l3d_mesh));
    m->bones = 0;
    m->bone_names = 0;
    m->indexed = false;
    m->indicies = 0;
    m->vbo = vbo;
    m->verts = out;
    m->vertex_count = i;
    m->bsphere.center[0] = 0;
    m->bsphere.center[1] = 0;
    m->bsphere.center[2] = 0;
    m->bsphere.rad = sx > sy ? sx : sy;
    m->bsphere.rad = sz > m->bsphere.rad ? sz : m->bsphere.rad;
    return m;
}
