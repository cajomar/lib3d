#include "internal.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>


L3D_EXPORTED
struct l3d_scene*
l3d_scene_new(void) {
    if (l3d_context__ == NULL)
        return NULL;
    struct l3d_scene* scene = malloc(sizeof(struct l3d_scene));
    scene->view.translate[0] = 0;
    scene->view.translate[1] = 1;
    scene->view.translate[2] = 0;
    scene->view.look_at[0] = 0;
    scene->view.look_at[1] = -1;
    scene->view.look_at[2] = 0;
    mat4x4_perspective(scene->view.projection, 45,
            l3d_context__->main_framebuffer.tex.w/(float)l3d_context__->main_framebuffer.tex.h,
            0.01, 64);
    scene->models = NULL;
    return scene;
}

L3D_EXPORTED
void
l3d_scene_delete(struct l3d_scene* scene) {
    sbforeachv(struct l3d_model* m, scene->models) {
        m->scene = NULL;
    }
    sbfree(scene->models);
    free(scene);
}

L3D_EXPORTED
struct l3d_view*
l3d_scene_view(struct l3d_scene* scene) {
    return &scene->view;
}

L3D_EXPORTED
void
l3d_scene_set_view(struct l3d_scene* scene, struct l3d_view* v) {
    memcpy(&scene->view, v, sizeof(struct l3d_view));
}

L3D_EXPORTED
void
l3d_shader_set_uniform(struct l3d_shader* shader, const char* key, int comps, float* value) {
    glUseProgram(shader->id);
    GLint h = glGetUniformLocation(shader->id, key);
    switch (comps) {
        case 1: glUniform1f(h, value[0]); break;
        case 2: glUniform2f(h, value[0], value[1]); break;
        case 3: glUniform3f(h, value[0], value[1], value[2]); break;
        case 4: glUniform4f(h, value[0], value[1], value[2], value[3]); break;
    }
}

struct render_state {
    struct l3d_view* view;
    mat4x4 pv_matrix;
    mat4x4 view_matrix;
    int pass;
    int viewport_w;
    int viewport_h;
    float time;
};

static
void
render_model_slot(struct l3d_model* model, struct i_l3d_model_slot* slot, struct render_state* state) {
    if (!slot->mesh)
        return;
    if (sbcount(slot->shaders) <= state->pass)
        return;
    struct l3d_shader* shader = slot->shaders[state->pass];
    if (!shader || shader->a_position < 0) {
        return;
    }
    glUseProgram(shader->id);

    for (int i=0; i<sbcount(shader->texture_slots); i++) {
        if (i < sbcount(slot->textures) && shader->texture_slots[i] != -1 && slot->textures[i]) {
            glActiveTexture(GL_TEXTURE0+i);
            glBindTexture(GL_TEXTURE_2D, slot->textures[i]->ptr);
            glUniform1i(shader->texture_slots[i], i);
        }
    }

    if (shader->u_pv >= 0)
        glUniformMatrix4fv(shader->u_pv, 1, GL_FALSE, (float*)state->pv_matrix);
    if (shader->u_pv_inverse >= 0) {
        mat4x4 T;
        mat4x4_invert(T, state->pv_matrix);
        glUniformMatrix4fv(shader->u_pv_inverse, 1, GL_FALSE, (float*)T);
    }
    if (model->tess_level >= 0.1) {
        glUniform1f(shader->u_tess_level, model->tess_level);
    }
    if (shader->u_view_matrix >= 0) {
        glUniformMatrix4fv(shader->u_view_matrix, 1, GL_FALSE, (float*)state->view_matrix);
    }
    if (shader->u_projection_matrix >= 0) {
        glUniformMatrix4fv(shader->u_projection_matrix, 1, GL_FALSE,
                (float*)state->view->projection);
    }
    if (shader->u_time >= 0)
        glUniform1f(shader->u_time, state->time);
    if (shader->u_camera >= 0)
        glUniform3fv(shader->u_camera, 1, state->view->translate);
    if (shader->u_pixel_size >= 0)
        glUniform2f(shader->u_pixel_size,
                1.0/state->viewport_w,
                1.0/state->viewport_h);

    if (shader->u_model_matrix >= 0) {
        mat4x4 t;
        mat4x4_mul(t, model->transform, slot->transform);
        glUniformMatrix4fv(shader->u_model_matrix, 1, GL_FALSE, (float*)t);
    }

    if (shader->u_bones >= 0) {
        mat4x4 bones[100];
        memset((float*)bones, 0, 100 * 16);
        memcpy((float*)bones, slot->mesh->bones, sbcount(slot->mesh->bones));
        glUniformMatrix4fv(shader->u_bones, 100, GL_FALSE, (float*)bones);
    }

    //if (shader->u_color >= 0) {
    //    glUniform4fv(shader->u_color, 1, state->color);
    //}

    glBindBuffer(GL_ARRAY_BUFFER, slot->mesh->vbo);
    glVertexAttribPointer(shader->a_position,
            3, GL_FLOAT, GL_FALSE, sizeof(struct l3d_vertex),
            (char*)NULL + 0);
    if (shader->a_normal >= 0) {
        glVertexAttribPointer(shader->a_normal,
            3, GL_FLOAT, GL_FALSE, sizeof(struct l3d_vertex),
            (char*)NULL + (3)*sizeof(float));
    }
    if (shader->a_tangent >= 0) {
        glVertexAttribPointer(shader->a_tangent,
                3, GL_FLOAT, GL_FALSE, sizeof(struct l3d_vertex),
                (char*)NULL + (3+3)*sizeof(float));
    }
    if (shader->a_uv >= 0) {
        glVertexAttribPointer(shader->a_uv,
            2, GL_FLOAT, GL_FALSE, sizeof(struct l3d_vertex),
            (char*)NULL + (3+3+3)*sizeof(float));
    }
    if (shader->a_bone_weights >= 0) {
        glVertexAttribPointer(shader->a_bone_weights,
            4, GL_FLOAT, GL_FALSE, sizeof(struct l3d_vertex),
            (char*)NULL + (3+3+3+2)*sizeof(float));
    }
    if (shader->a_bone_ids >= 0) {
        glVertexAttribPointer(shader->a_bone_ids,
            4, GL_UNSIGNED_SHORT, GL_FALSE, sizeof(struct l3d_vertex),
            (char*)NULL + (3+3+3+2+4)*sizeof(float));
    }
    glEnableVertexAttribArray(shader->a_position);
    if (shader->a_normal >= 0) {
        glEnableVertexAttribArray(shader->a_normal);
    }
    if (shader->a_tangent >= 0) {
        glEnableVertexAttribArray(shader->a_tangent);
    }
    if (shader->a_uv >= 0) {
        glEnableVertexAttribArray(shader->a_uv);
    }
    if (shader->a_bone_ids >= 0) {
        glEnableVertexAttribArray(shader->a_bone_ids);
    }
    if (shader->a_bone_weights >= 0) {
        glEnableVertexAttribArray(shader->a_bone_weights);
    }

    if (model->instances) {
        int attrs_count = sbcount(model->attr_sizes);
        int count = sbcount(model->instances[0])/model->attr_sizes[0];
        if (!model->instance_vbos) {
            GLuint* vbos = sbadd(model->instance_vbos, attrs_count);
            glGenBuffers(attrs_count, vbos);
        }
        for (int i=0; i<attrs_count; i++) {
            glBindBuffer(GL_ARRAY_BUFFER, model->instance_vbos[i]);
            int comp_count = model->attr_sizes[i];
            if (model->instance_dirty) {
                float* data = model->instances[i];
                glBufferData(GL_ARRAY_BUFFER, comp_count*sizeof(float)*count, data, GL_STATIC_DRAW);
            }
            // TODO Cache attrib location
            GLuint loc = glGetAttribLocation(shader->id, model->attr_names[i]);
            if (loc != -1) {
                glEnableVertexAttribArray(loc);
                glVertexAttribPointer(loc, comp_count, GL_FLOAT, GL_FALSE, comp_count*sizeof(float), (GLvoid*)0);
                glVertexAttribDivisor(loc, 1);
            } else {
                printf("Failed to find shader location: %s\n", model->attr_names[i]);
            }
        }
        model->instance_dirty = false;
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    if (slot->mesh->indexed) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, slot->mesh->index_vbo);
        if (shader->draw_patches) {
#ifdef GLMAX
            glDrawElements(GL_PATCHES, slot->mesh->index_count, GL_UNSIGNED_SHORT, (void*)0);
#endif
        } else {
            if (model->instances) {
                int count = sbcount(model->instances[0])/model->attr_sizes[0];
                glDrawElementsInstanced(GL_TRIANGLES, slot->mesh->index_count, GL_UNSIGNED_SHORT, (void*)0, count);
            } else {
                glDrawElements(GL_TRIANGLES, slot->mesh->index_count, GL_UNSIGNED_SHORT, (void*)0);
            }
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    } else {
        if (shader->draw_patches) {
#ifdef GLMAX
            glDrawArrays(GL_PATCHES, 0, slot->mesh->vertex_count);
#endif
        } else {
            if (model->instances) {
                int count = sbcount(model->instances[0])/model->attr_sizes[0];
                glDrawArraysInstanced(GL_TRIANGLES, 0, slot->mesh->vertex_count, count);
            } else {
                glDrawArrays(GL_TRIANGLES, 0, slot->mesh->vertex_count);
            }
        }
    }

    glDisableVertexAttribArray(shader->a_position);
    if (shader->a_normal >= 0)
        glDisableVertexAttribArray(shader->a_normal);
    if (shader->a_tangent >= 0)
        glDisableVertexAttribArray(shader->a_tangent);
    if (shader->a_uv >= 0)
        glDisableVertexAttribArray(shader->a_uv);
    if (shader->a_bone_ids >= 0)
        glDisableVertexAttribArray(shader->a_bone_ids);
    if (shader->a_bone_weights >= 0)
        glDisableVertexAttribArray(shader->a_bone_weights);

    int attrs_count = sbcount(model->attr_sizes);
    for (int i=0; i<attrs_count; i++) {
        GLuint loc = glGetAttribLocation(shader->id, model->attr_names[i]);
        glVertexAttribDivisor(loc, 0);
        glDisableVertexAttribArray(loc);
    }
}

static
void
render_model(struct l3d_model* model, struct render_state* state) {
    sbforeachp(struct i_l3d_model_slot* s, model->slots) {
        render_model_slot(model, s, state);
    }
}

L3D_EXPORTED
void
l3d_scene_render(struct l3d_scene* scene, struct l3d_scene_render_conf* conf) {
    if (l3d_context__ == NULL)
        return;
    struct l3d_fbo* target = conf->target;
    if (target == NULL) {
        target = &l3d_context__->main_framebuffer;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, target->fbo);
    if (conf->clear) {
        glClearColor(target->r, target->g, target->b, target->a);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    glViewport(0, 0, target->tex.w, target->tex.h);

    struct render_state state;
    state.view = &scene->view;
    state.time = (conf->time_past % 1048576llu) / 1048576.0;

    {
        float eye[3];
        eye[0] = 0;
        eye[1] = 0;
        eye[2] = 0;
        float up[3];
        up[0] = 0;
        up[1] = 0;
        up[2] = 1;
        mat4x4_look_at(state.view_matrix, eye, state.view->look_at, up);
        mat4x4_translate_in_place(state.view_matrix,
                state.view->translate[0],
                state.view->translate[1],
                state.view->translate[2]);
        mat4x4_mul(state.pv_matrix, state.view->projection, state.view_matrix);
    }

    state.pass = conf->pass;
    state.viewport_w = target->tex.w;
    state.viewport_h = target->tex.h;

    if (conf->cull_face)
        glEnable(GL_CULL_FACE);
    else
        glDisable(GL_CULL_FACE);
    if (conf->blend) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    } else {
        glDisable(GL_BLEND);
    }
    if (conf->depth_test) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
    } else {
        glDisable(GL_DEPTH_TEST);
    }
    if (conf->depth_writable) {
        glDepthMask(GL_TRUE);
    } else {
        glDepthMask(GL_FALSE);
    }
    sbforeachv(struct l3d_model* m, scene->models) {
        render_model(m, &state);
    }
}

L3D_EXPORTED
int
l3d_scene_add_model(struct l3d_scene* scene, struct l3d_model* m) {
    if (m->scene) {
        assert(false);
        return 1;
    }
    sbpush(scene->models, m);
    m->scene = scene;
    return 1;
}

L3D_EXPORTED
int
l3d_scene_remove_model(struct l3d_scene* scene, struct l3d_model* m) {
    int found = 0;
    sbforeachv(struct l3d_model* fm, scene->models) {
        if (m == fm)
            break;
        found ++;
    }
    if (found < sbcount(scene->models)) {
        scene->models[found]->scene = NULL;
        sbremove(scene->models, found, 1);
        return 1;
    } else {
        assert(false);
        return 0;
    }
}

float
dot(float* a, float* b) {
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

static
float
dist(vec3 a, vec3 b) {
    vec3 s;
    vec3_sub(s, a,  b);
    return fabs(vec3_len(s));
}

static
bool
intersect_triangle(struct l3d_vertex* a, struct l3d_vertex* b, struct l3d_vertex* c, vec3 ray_orig, vec3 ray, struct l3d_intersection* intersection) {
    // from http://www.geometrictools.com/GTEngine/Include/Mathematics/GteIntrRay3Triangle3.h

    vec3 edge1;
    vec3 edge2;
    vec3_sub(edge1, b->position, a->position);
    vec3_sub(edge2, c->position, a->position);

    vec3 normal;
    vec3_mul_cross(normal, edge1, edge2);

    float DdN = -dot(ray, normal);
    if (DdN < -FLT_EPSILON) return false;

    float sign = -1.f;

    vec3 diff;
    vec3_sub(diff, ray_orig, a->position);

    vec3 d_;
    vec3_mul_cross(d_, diff, edge2);
    float DdQxE2 = sign * dot(ray, d_);
    if (DdQxE2 >= FLT_EPSILON) {
        vec3_mul_cross(d_, edge1, diff);
        float DdE1xQ = sign * dot(ray, d_);
        if (DdE1xQ >= FLT_EPSILON) {
            if (DdQxE2 + DdE1xQ <= DdN) {
                // Line intersects triangle, check whether ray does.
                float QdN = -sign * dot(diff, normal);
                if (QdN >= FLT_EPSILON) {
                    // Ray intersects triangle.
                    float inv = 1.f / DdN;
                    float parameter = QdN*inv;

                    vec3 pos;
                    vec3_scale(d_, ray, parameter);
                    vec3_add(pos, ray_orig, d_);

                    if (!intersection->model || dist(intersection->position, ray_orig) > dist(pos, ray_orig)) {
                        memcpy(intersection->position, pos, sizeof(float)*3);

                        intersection->triangle_bary[1] = DdQxE2*inv;
                        intersection->triangle_bary[2] = DdE1xQ*inv;
                        intersection->triangle_bary[0] = 1.f - intersection->triangle_bary[1] - intersection->triangle_bary[2];
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

L3D_EXPORTED
struct l3d_intersection*
l3d_scene_intersect_screen(struct l3d_scene* scene, int mx, int my, int* count) {
    int vp_width = l3d_context__->main_framebuffer.tex.w;
    int vp_height = l3d_context__->main_framebuffer.tex.h;

    *count = 0;
    struct l3d_intersection* intersections = 0;

    vec4 ray_clip;
    ray_clip[0] = (2.f*mx) / vp_width - 1.f;
    ray_clip[1] = 1.f - (2.f*my) / vp_height;
    ray_clip[2] = -1.f;
    ray_clip[3] = 1.f;

    float eye[3]; eye[0] = 0; eye[1] = 0; eye[2] = 0;
    float up[3]; up[0] = 0; up[1] = 0; up[2] = 1;
    mat4x4 view_matrix;
    mat4x4_look_at(view_matrix, eye, scene->view.look_at, up);

    mat4x4 inv_proj;
    mat4x4_invert(inv_proj, scene->view.projection);

    mat4x4 inv_view;
    mat4x4_invert(inv_view, view_matrix);

    // To eye coordinates
    vec4 ray_eye;
    mat4x4_mul_vec4(ray_eye, inv_proj, ray_clip);
    ray_eye[2] = -1.f;
    ray_eye[3] = 0.f;

    // To world coordinates
    vec4 ray_wor;
    mat4x4_mul_vec4(ray_wor, inv_view, ray_eye);
    vec3_norm(ray_wor, ray_wor);
    ray_wor[3] = 0.f;

    vec4 ray_origin_wor;
    ray_origin_wor[0] = -scene->view.translate[0];
    ray_origin_wor[1] = -scene->view.translate[1];
    ray_origin_wor[2] = -scene->view.translate[2];
    ray_origin_wor[3] = 1.f;

    sbforeachv(struct l3d_model* model, scene->models) {
        sbforeachp(struct i_l3d_model_slot* s, model->slots) {
            struct l3d_mesh* m = s->mesh;
            struct i_sphere* sp = &m->bsphere;

            // TODO slot transform!

            // Convert ray and ray origin to object space
            mat4x4 inv_model;
            mat4x4_invert(inv_model, model->transform);

            vec4 ray_obj;
            mat4x4_mul_vec4(ray_obj, inv_model, ray_wor);
            vec3_norm(ray_obj, ray_obj);
            ray_obj[3] = 1.f;

            vec3 ray_origin_obj;
            mat4x4_mul_vec4(ray_origin_obj, inv_model, ray_origin_wor);

            // sphere intersection test
            vec3 dist; vec3_sub(dist, ray_origin_obj, sp->center);
            float b = dot(ray_obj, dist);
            float c = dot(dist, dist) - sp->rad * sp->rad;
            float bsmc = b * b - c;
            float sbsmc = sqrtf(bsmc);
            if (bsmc < -FLT_EPSILON) {
                continue;
            }
            float intersection_distance;
            if (bsmc > FLT_EPSILON) {
                float t_a = -b + sbsmc;
                float t_b = -b - sbsmc;
                intersection_distance = t_b;
                if (t_a < -FLT_EPSILON && t_b < -FLT_EPSILON) {
                    continue;
                }
            } else {
                continue;
            }
            //printf("%f\n", intersection_distance);

            // We probably intersect, find triangle.
            if (m->indexed) {
                struct l3d_intersection in;
                in.model = 0;
                for (int i=0; i<m->face_count; i++) {
                    struct l3d_vertex a = m->verts[m->indicies[i*3 + 0]];
                    struct l3d_vertex b = m->verts[m->indicies[i*3 + 1]];
                    struct l3d_vertex c = m->verts[m->indicies[i*3 + 2]];
                    if (intersect_triangle(&a, &b, &c,
                            ray_origin_obj, ray_obj,
                            &in)) {
                        in.model = model;
                        in.mesh = m;
                        in.face_index = i;
                    }
                }
                if (in.model) {
                    (*count) = (*count)+1;
                    intersections = realloc(intersections, (*count)*sizeof(struct l3d_intersection));
                    memcpy(&intersections[(*count)-1], &in, sizeof(struct l3d_intersection));
                }
            } else {
                // TODO
                assert(false);
                continue;
            }
        }
    }

    return intersections;
}

L3D_EXPORTED
void
l3d_release_intersections(struct l3d_intersection* i) {
    free(i);
}
