#include "internal.h"

L3D_EXPORTED
struct l3d_fbo*
l3d_fbo_new(int w, int h, uint32_t fbo_flags, uint32_t texture_flags) {
    GLuint tex, fbo, depth_tex = 0;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    if (texture_flags & l3d_TEXTURE_CLAMP_X)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    else
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

    if (texture_flags & l3d_TEXTURE_CLAMP_Y)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    else
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    if (fbo_flags & l3d_FBO_R) {
        if (fbo_flags & l3d_FBO_HDR16)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
        else if (fbo_flags & l3d_FBO_HDR32)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
        else
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
    } else {
        if (fbo_flags & l3d_FBO_HDR16)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        else if (fbo_flags & l3d_FBO_HDR32)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        else
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    }
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
    
    if (fbo_flags & l3d_FBO_DEPTH) {
        glGenTextures(1, &depth_tex);
        glBindTexture(GL_TEXTURE_2D, depth_tex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 
                            w, h, 
                            0, GL_DEPTH_COMPONENT, GL_FLOAT,
                            NULL); 
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_tex, 0);
    }

    struct l3d_fbo* b = malloc(sizeof(struct l3d_fbo));
    b->tex.ptr = tex;
    b->tex.w = w;
    b->tex.h = h;
    b->depth_tex.ptr = depth_tex;
    b->depth_tex.w = w;
    b->depth_tex.h = h;
    b->fbo = fbo;
    b->r = 0;
    b->g = 0;
    b->b = 0;
    b->a = 0;
    return b;
}

L3D_EXPORTED
void
l3d_fbo_delete(struct l3d_fbo* fbo) {
    if (fbo->fbo)
        glDeleteBuffers(1, &fbo->fbo);
    if (fbo->tex.ptr)
        glDeleteTextures(1, &fbo->tex.ptr);
    if (fbo->depth_tex.ptr)
        glDeleteTextures(1, &fbo->depth_tex.ptr);
    free(fbo);
}

L3D_EXPORTED
void
l3d_fbo_set_clear_color(struct l3d_fbo* fbo, float r, float g, float b, float a) {
    fbo->r = r;
    fbo->g = g;
    fbo->b = b;
    fbo->a = a;
}

L3D_EXPORTED
struct l3d_texture*
l3d_fbo_reader(struct l3d_fbo* fbo) {
    return &fbo->tex;
}

L3D_EXPORTED
struct l3d_texture*
l3d_fbo_depth_reader(struct l3d_fbo* fbo) {
    return &fbo->depth_tex;
}
