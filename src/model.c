#include "internal.h"
#include <string.h>
#include <stdio.h>
#include <float.h>

L3D_EXPORTED
struct l3d_model*
l3d_model_new(void) {
    struct l3d_model* m = malloc(sizeof(struct l3d_model));
    m->scene = NULL;
    m->slots = NULL;
    mat4x4_identity(m->transform);
    m->tess_level = 0;
    m->instance_vbos = 0;
    m->instances = 0;
    m->attr_sizes = 0;
    m->attr_names = 0;
    return m;
}

L3D_EXPORTED
void
l3d_model_delete(struct l3d_model* m) {
    if (m->instance_vbos) {
        glDeleteBuffers(sbcount(m->instance_vbos), m->instance_vbos);
        sbfree(m->instance_vbos);
    }

    sbforeachp(struct i_l3d_model_slot* slot, m->slots) {
        sbfree(slot->textures);
        sbfree(slot->shaders);
    }
    sbfree(m->slots);
    if (m->scene) {
        l3d_scene_remove_model(m->scene, m);
    }
    m->slots = NULL;
    m->scene = NULL;
    for (int i=0; i<sbcount(m->attr_sizes); i++) {
        sbfree(m->instances[i]);
    }
    free(m->instances);
    sbfree(m->attr_sizes);
    sbforeachv(char* i, m->attr_names) {
        free(i);
    }
    sbfree(m->attr_names);
    if (m->attr_sizes) {
        sbfree(m->attr_sizes);
    }
    free(m);
}

L3D_EXPORTED
void
l3d_model_set_transform(struct l3d_model* m, mat4x4 T) {
    memcpy(m->transform, T, sizeof(mat4x4));
}

static
struct i_l3d_model_slot*
get_or_make_slot(struct l3d_model* m, int mesh_slot) {
    assert(mesh_slot >= 0);
    while (sbcount(m->slots) <= mesh_slot) {
        struct i_l3d_model_slot* slot = sbadd(m->slots, 1);
        slot->mesh = NULL;
        slot->textures = NULL;
        slot->shaders = NULL;
        mat4x4_identity(slot->transform);
    }
    return &m->slots[mesh_slot];
}

L3D_EXPORTED
void
l3d_model_set_mesh(struct l3d_model* m, int mesh_slot, struct l3d_mesh* mesh) {
    struct i_l3d_model_slot* slot = get_or_make_slot(m, mesh_slot);
    slot->mesh = mesh;
}

L3D_EXPORTED
void
l3d_model_remove_mesh(struct l3d_model* m, int mesh_slot) {
    struct i_l3d_model_slot* slot = get_or_make_slot(m, mesh_slot);
    slot->mesh = NULL;
}

L3D_EXPORTED
void
l3d_model_set_mesh_transform(struct l3d_model* m, int mesh_slot, mat4x4 T) {
    struct i_l3d_model_slot* slot = get_or_make_slot(m, mesh_slot);
    memcpy(slot->transform, T, sizeof(mat4x4));
}

L3D_EXPORTED
void
l3d_model_set_texture(struct l3d_model* m, int mesh_slot, int texture_slot, struct l3d_texture* tex) {
    assert(tex);
    assert(texture_slot >= 0);
    if (mesh_slot > -1) {
        struct i_l3d_model_slot* slot = get_or_make_slot(m, mesh_slot);
        while (sbcount(slot->textures) <= texture_slot) {
            sbpush(slot->textures, NULL);
        }
        slot->textures[texture_slot] = tex;
    } else {
        sbforeachp(struct i_l3d_model_slot* slot, m->slots) {
            while (sbcount(slot->textures) <= texture_slot) {
                sbpush(slot->textures, NULL);
            }
            slot->textures[texture_slot] = tex;
        }
    }
}

L3D_EXPORTED
void
l3d_model_set_shader(struct l3d_model* m, struct l3d_shader* shader, int mesh_slot, int pass) {
    assert(pass >= 0);
    assert(mesh_slot >= -1);
    if (mesh_slot > -1) {
        struct i_l3d_model_slot* slot = get_or_make_slot(m, mesh_slot);
        while (sbcount(slot->shaders) <= pass) {
            sbpush(slot->shaders, NULL);
        }
        slot->shaders[pass] = shader;
    } else {
        sbforeachp(struct i_l3d_model_slot* slot, m->slots) {
            while (sbcount(slot->shaders) <= pass) {
                sbpush(slot->shaders, NULL);
            }
            slot->shaders[pass] = shader;
        }
    }
}


L3D_EXPORTED
void
l3d_model_set_tess_level(struct l3d_model* m, float tess_level) {
    m->tess_level = tess_level;
}


L3D_EXPORTED
int
l3d_model_instance(struct l3d_model* m) {
    int attrs_count = sbcount(m->attr_sizes);
    if (attrs_count == 0) {
        printf("Can't create instance of model when no instance attributes have been created.\n");
        assert(false);
        return -1;
    }
    if (!m->instances) {
        m->instances = calloc(attrs_count, sizeof(float*));
    }
    int id = sbcount(m->instances[0]) / m->attr_sizes[0];
    for (int i=0; i<attrs_count; i++) {
        int size = m->attr_sizes[i];
        float* inst = sbadd(m->instances[i], size);
        memset(inst, 0, sizeof(float)*size);
    }
    m->instance_dirty = true;
    return id;
}

L3D_EXPORTED
int
l3d_model_new_instance_attr(struct l3d_model* m, const char* name, int component_count) {
    if (sbcount(m->instances) != 0) {
        printf("Can't create new instance attributes after a model has been instanced.\n");
        assert(false);
        return -1;
    }
    m->instance_dirty = true;
    int id = sbcount(m->attr_sizes);
    sbpush(m->attr_sizes, component_count);
    char* s = malloc(strlen(name)+1);
    strcpy(s, name);
    sbpush(m->attr_names, s);
    return id;
}

L3D_EXPORTED
void
l3d_instance_attr(struct l3d_model* m, int instance_id, int attr_id, float* v) {
    m->instance_dirty = true;
    int size = m->attr_sizes[attr_id];
    float* arr = m->instances[attr_id];
    memcpy(arr+(size*instance_id), v, sizeof(float)*size);
}

L3D_EXPORTED
void
l3d_model_get_bounding_sphere(struct l3d_model* m, struct i_sphere* out) {
    float min_x = FLT_MAX;
    float min_y = FLT_MAX;
    float min_z = FLT_MAX;
    float max_x = FLT_MIN;
    float max_y = FLT_MIN;
    float max_z = FLT_MIN;
    sbforeachp(struct i_l3d_model_slot* s, m->slots) {
        struct i_sphere c = s->mesh->bsphere;
        float xl = c.center[0] - c.rad;
        float xh = c.center[0] - c.rad;
        float yl = c.center[1] - c.rad;
        float yh = c.center[1] - c.rad;
        float zl = c.center[2] - c.rad;
        float zh = c.center[2] - c.rad;
        if (xh > max_x) max_x = xh;
        if (xl < min_x) min_x = xl;
        if (yh > max_y) max_y = yh;
        if (yl < min_y) min_y = yl;
        if (zh > max_z) max_z = zh;
        if (zl < min_z) min_z = zl;
    }
    float rad_x = (max_x-min_x)/2;
    float rad_y = (max_y-min_y)/2;
    float rad_z = (max_z-min_z)/2;
    out->center[0] = min_x + rad_x;
    out->center[1] = min_y + rad_y;
    out->center[2] = min_z + rad_z;
    out->rad = rad_x > rad_y ? rad_x : rad_y;
    out->rad = rad_z > out->rad ? rad_z : out->rad;
}
