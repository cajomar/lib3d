#include <stdlib.h>
#include "internal.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE
#define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT 0x84FF

L3D_EXPORTED
struct l3d_texture*
l3d_texture_from_path(const char* path, uint32_t flags) {
    int w,h,n;
    unsigned char* data = stbi_load(path, &w, &h, &n, 0);
    if (!data) {
        printf("Failed to find image at: %s\n", path);
        assert(0);
        return NULL;
    }
    struct l3d_texture* t = l3d_texture_from_data(data, w, h, n, flags);
    stbi_image_free(data);
    return t;
}

L3D_EXPORTED
struct l3d_texture*
l3d_texture_from_data(uint8_t* data, int w, int h, int bpp, uint32_t flags) {
    static GLfloat max_anisotropy = -1;
    if (max_anisotropy < 0)
        glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_anisotropy);
    if (max_anisotropy < 1)
        flags ^= l3d_TEXTURE_ANISOTROPY;

    GLuint ptr;
    glGenTextures(1, &ptr);
    glBindTexture(GL_TEXTURE_2D, ptr);

    if (flags & l3d_TEXTURE_CLAMP_X)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    else
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

    if (flags & l3d_TEXTURE_CLAMP_Y)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    else
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    if (flags & l3d_TEXTURE_MIPMAP)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    else
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GLuint f;
    switch (bpp) {
        case 1:
            f = GL_RED;
            break;
        case 3:
            f = GL_RGB;
            break;
        case 4:
            f = GL_RGBA;
            break;
        default:
            printf("l3d_texture_from_data: only 1, 3, or 4 bpp supported.\n");
            assert(0);
            glDeleteTextures(1, &ptr);
            return NULL;
    }
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, f, w, h, 0, f, GL_UNSIGNED_BYTE, data);

    if (flags & l3d_TEXTURE_MIPMAP)
        glGenerateMipmap(GL_TEXTURE_2D);

    if ((flags & l3d_TEXTURE_ANISOTROPY) && max_anisotropy >= 1)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_anisotropy);

    struct l3d_texture* texture = malloc(sizeof(struct l3d_texture));
    texture->ptr = ptr;
    texture->w = w;
    texture->h = h;
    return texture;
}

L3D_EXPORTED
void
l3d_texture_delete(struct l3d_texture* texture) {
    glDeleteTextures(1, &texture->ptr);
    free(texture);
}

L3D_EXPORTED
int
l3d_texture_width(struct l3d_texture* texture) {
    return texture->w;
}

L3D_EXPORTED
int
l3d_texture_height(struct l3d_texture* texture) {
    return texture->h;
}
