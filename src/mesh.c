#include "internal.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <float.h>

struct reader {
    uint8_t* s;
    uint32_t offset;
};

static
uint8_t
read_u8(struct reader* r) {
    const uint8_t* d = (uint8_t*)(r->s + r->offset);
    r->offset += 1;
    return d[0];
}

static
uint16_t
read_u16(struct reader* r) {
    const uint8_t* d = (uint8_t*)(r->s + r->offset);
    r->offset += 2;

    union {
        uint8_t bytes[2];
        uint16_t u;
    } fun = { .bytes = {d[0], d[1]} };
    return fun.u;
}

static
uint16_t*
read_n_u16(struct reader* r, int n) {
    // Only works on little-endian systems.
    uint8_t* d = (uint8_t*)(r->s + r->offset);
    r->offset += n*2;
    return (uint16_t*)d;
}

static
uint32_t
read_u32(struct reader* r) {
    const uint8_t* d = (uint8_t*)(r->s + r->offset);
    r->offset += 4;

    union {
        uint8_t bytes[4];
        uint32_t u;
    } fun = { .bytes = {d[0], d[1], d[2], d[3]} };
    return fun.u;
}

static
float
read_f32(struct reader* r) {
    const uint8_t* d = (uint8_t*)(r->s + r->offset);
    r->offset += 4;

    union {
        uint8_t bytes[4];
        float f;
    } fun = { .bytes = {d[0], d[1], d[2], d[3]} };
    assert(!isnan(fun.f));
    assert(!isinf(fun.f));
    return fun.f;
}

static
l3d_ident
read_string(struct reader* r) {
    uint32_t len = read_u32(r);
    r->offset += len;
    return l3d_ident_from_strn((const char*) ((r->s+r->offset)-len), len);
}

L3D_EXPORTED
struct l3d_mesh_bank*
l3d_mesh_bank_load(const char* path) {
    uint8_t* buffer = NULL;
    FILE *infile = fopen(path, "rb");
    if (!infile) {
        printf("Unable to open input file '%s'.\n", path);
        goto L_ERROR;
    }
    fseek(infile, 0, SEEK_END);
    uint32_t length = ftell(infile);
    if (length < 4) {
        printf("File '%s' too short\n", path);
        goto L_ERROR;
    }
    buffer = malloc(length+1);
    buffer[length] = '\0';
    fseek(infile, 0, SEEK_SET);
    uint32_t bytes_read = fread((char*)buffer, 1, length, infile);
    assert(bytes_read == length);
    fclose(infile); infile = 0;

    struct reader r;
    r.s = buffer;
    r.offset = 0;

    struct l3d_mesh_bank* bank = malloc(sizeof(struct l3d_mesh_bank));
    bank->entries = NULL;

    uint32_t num_models = read_u32(&r);

    for (int i=0; i<num_models; i++) {
        l3d_ident model_name = read_string(&r);

        int children = 1;
        while (children) {
            uint8_t num_meshs = read_u8(&r);
            for (int k=0; k<num_meshs; k++) {
                struct i_l3d_mesh_bank_entry* entry = sbadd(bank->entries, 1);
                entry->model_name = model_name;
                entry->mesh = malloc(sizeof(struct l3d_mesh));

                uint16_t num_verts = read_u16(&r);
                uint16_t num_faces = read_u16(&r);
                uint16_t num_bones = read_u16(&r);

                entry->mesh->vertex_count = num_verts;
                entry->mesh->index_count = num_faces * 3;
                entry->mesh->indexed = true;
                entry->mesh->bones = 0;
                entry->mesh->bone_names = 0;
                sbadd(entry->mesh->bones, num_bones*16);
                sbadd(entry->mesh->bone_names, num_bones);

                struct l3d_vertex* out = malloc(sizeof(struct l3d_vertex) * num_verts);
                for (int j=0; j<num_verts; j++) {
                    struct l3d_vertex* v = &out[j];
                    v->position[0] = read_f32(&r);
                    v->position[1] = read_f32(&r);
                    v->position[2] = read_f32(&r);
                    v->normal[0] = read_f32(&r);
                    v->normal[1] = read_f32(&r);
                    v->normal[2] = read_f32(&r);
                    v->tangent_normal[0] = read_f32(&r);
                    v->tangent_normal[1] = read_f32(&r);
                    v->tangent_normal[2] = read_f32(&r);
                    v->u = read_f32(&r);
                    v->v = read_f32(&r);
                    v->bone_weights[0] = read_f32(&r);
                    v->bone_weights[1] = read_f32(&r);
                    v->bone_weights[2] = read_f32(&r);
                    v->bone_weights[3] = read_f32(&r);
                    v->bone_ids[0] = read_u16(&r);
                    v->bone_ids[1] = read_u16(&r);
                    v->bone_ids[2] = read_u16(&r);
                    v->bone_ids[3] = read_u16(&r);
                }

                GLushort* indicies = malloc(sizeof(GLushort) * num_faces * 3);
                for (int j=0; j<num_faces*3; j++) {
                    indicies[j] = read_u16(&r);
                }


                for (int j=0; j<num_bones; j++) {
                    entry->mesh->bone_names[j] = read_string(&r);
                }
                for (int j=0; j<num_bones*16; j++) {
                    entry->mesh->bones[j] = read_f32(&r);
                }


                glGenBuffers(2, &entry->mesh->vbo);

                glBindBuffer(GL_ARRAY_BUFFER, entry->mesh->vbo);
                glBufferData(GL_ARRAY_BUFFER, sizeof(struct l3d_vertex)*num_verts, out, GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entry->mesh->index_vbo);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)*num_faces*3, indicies, GL_STATIC_DRAW);

                i_mesh_calculate_bsphere(&entry->mesh->bsphere, out, num_verts);
                entry->mesh->verts = out;
                entry->mesh->indicies = indicies;
                entry->mesh->face_count = num_faces;
            }
            children --;
            children += read_u8(&r);
        }


        uint32_t num_animations = read_u32(&r);
    }

    free(buffer);
    return bank;
    
L_ERROR:
    if (buffer)
        free(buffer);
    if (infile)
        fclose(infile);
    return NULL;
}

L3D_EXPORTED
struct l3d_model*
l3d_mesh_bank_get(struct l3d_mesh_bank* bank, l3d_ident model_name) {
    struct l3d_model* m = l3d_model_new();
    int slot = 0;
    sbforeachp(struct i_l3d_mesh_bank_entry* e, bank->entries) {
        if (e->model_name == model_name) {
            l3d_model_set_mesh(m, slot, e->mesh);
            slot++;
        }
    }
    if (slot == 0) {
        printf("No mesh named '%s' in mesh bank.\n", l3d_ident_as_char(model_name));
        printf("Here are all the available meshes:\n");
        sbforeachp(struct i_l3d_mesh_bank_entry* e, bank->entries) {
            printf("  %s\n", l3d_ident_as_char(e->model_name));
        }
        assert(false);
    }
    return m;
}

L3D_EXPORTED
struct l3d_mesh*
l3d_mesh_new(struct l3d_vertex* vertices, uint32_t vertex_count, uint32_t mesh_flags) {
    if (mesh_flags & l3d_MESH_CALCULATE_TANGENTS) {
        i_calc_tangents(vertices, vertex_count);
    }

    struct l3d_mesh* mesh = malloc(sizeof(struct l3d_mesh));
    mesh->verts = malloc(sizeof(struct l3d_vertex)*vertex_count);
    memcpy(mesh->verts, vertices, sizeof(struct l3d_vertex)*vertex_count);
    mesh->vertex_count = vertex_count;
    mesh->indexed = false;
    mesh->indicies = 0;
    mesh->bones = 0;
    mesh->bone_names = 0;
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(struct l3d_vertex)*vertex_count, vertices, GL_STATIC_DRAW);
    mesh->vbo = vbo;
    i_mesh_calculate_bsphere(&mesh->bsphere, vertices, vertex_count);
    return mesh;
}

void
i_mesh_calculate_bsphere(struct i_sphere* s, struct l3d_vertex* verts, uint32_t count) {
    float min_x = FLT_MAX;
    float min_y = FLT_MAX;
    float min_z = FLT_MAX;
    float max_x = FLT_MIN;
    float max_y = FLT_MIN;
    float max_z = FLT_MIN;
    for (int i=0; i<count; i++) {
        float x = verts[i].position[0];
        float y = verts[i].position[1];
        float z = verts[i].position[2];
        if (x > max_x) max_x = x;
        if (x < min_x) min_x = x;
        if (y > max_y) max_y = y;
        if (y < min_y) min_y = y;
        if (z > max_z) max_z = z;
        if (z < min_z) min_z = z;
    }
    float rad_x = (max_x-min_x)/2;
    float rad_y = (max_y-min_y)/2;
    float rad_z = (max_z-min_z)/2;
    s->center[0] = min_x + rad_x;
    s->center[1] = min_y + rad_y;
    s->center[2] = min_z + rad_z;
    s->rad = rad_x > rad_y ? rad_x : rad_y;
    s->rad = rad_z > s->rad ? rad_z : s->rad;
}
