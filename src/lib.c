#include "internal.h"
#include "glsw.h"
#include <stdio.h>


struct l3d_context* l3d_context__ = NULL;

L3D_EXPORTED
int
l3d_init() {
    if (l3d_context__) {
        return 0;
    }
    int res = ogl_LoadFunctions();
    if (res != ogl_LOAD_SUCCEEDED) {
        printf("Failed to load opengl function: %d\n", res);
        return 0;
    }
    l3d_context__ = malloc(sizeof(struct l3d_context));
    l3d_context__->main_framebuffer.tex.w = 0;
    l3d_context__->main_framebuffer.tex.h = 0;
    l3d_context__->main_framebuffer.tex.ptr = 0;
    l3d_context__->main_framebuffer.depth_tex.ptr = 0;
    l3d_context__->main_framebuffer.fbo = 0;
    l3d_context__->main_framebuffer.r = 0;
    l3d_context__->main_framebuffer.g = 0;
    l3d_context__->main_framebuffer.b = 0;
    l3d_context__->main_framebuffer.a = 0;
    return glswInit();
}

L3D_EXPORTED
void
l3d_viewport(int w, int h) {
    if (l3d_context__) {
        l3d_context__->main_framebuffer.tex.w = w;
        l3d_context__->main_framebuffer.tex.h = h;
    }
}

L3D_EXPORTED
int
l3d_shutdown(void) {
    if (l3d_context__ == NULL)
        return 0;
    free(l3d_context__);
    l3d_context__ = NULL;
    return glswShutdown();
}

L3D_EXPORTED
int
l3d_set_shaders_path(const char* path_prefix, const char* path_suffix) {
    if (l3d_context__ == NULL)
        return 0;
    return glswSetPath(path_prefix, path_suffix);
}


L3D_EXPORTED
int
l3d_add_shader_directive(const char* token, const char* directive) {
    return glswAddDirectiveToken(token, directive);
}

void
i_debug_mat4(mat4x4 mat) {
    float* m = (float*) mat;
    printf("-----------------------------------\n"
           "%f %f %f %f\n"
           "%f %f %f %f\n"
           "%f %f %f %f\n"
           "%f %f %f %f\n"
           "-----------------------------------\n",
            m[0], m[1], m[2], m[3],
            m[4], m[5], m[6], m[7],
            m[8], m[9], m[10], m[11],
            m[12], m[13], m[14], m[15]);
}
