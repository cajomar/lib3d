#ifndef __LIB3D_INTERNAL__
#define __LIB3D_INTERNAL__
#include <lib3d.h>
#include "stretchy_buffer.h"

#ifdef GLMAX
#include "gl_core_max.h"
#else
#include "gl_core_3_3.h"
#endif

#include <stdbool.h>

struct l3d_scene {
    struct l3d_view view;
    struct l3d_model** models; // stretchy_buffer
};

struct l3d_shader {
    GLint id;
    GLint a_position, a_normal, a_tangent, a_uv, a_bone_ids, a_bone_weights;
    GLint u_pv, u_time, u_camera, u_pixel_size, u_color, u_model_matrix, u_view_matrix, u_projection_matrix, u_pv_inverse, u_tess_level, u_bones;
    GLint* texture_slots; // stretchy_buffer
    bool draw_patches;
};
struct i_l3d_model_slot {
    struct l3d_mesh* mesh;
    struct l3d_texture** textures; // stretchy_buffer
    struct l3d_shader** shaders; // stretchy_buffer indexed by `pass`
    mat4x4 transform;
};
struct l3d_model {
    struct l3d_scene* scene;
    struct i_l3d_model_slot* slots; // stretchy_buffer
    mat4x4 transform;
    float tess_level;
    GLuint* instance_vbos; // stretchy_buffer
    float** instances; // fixed size array of stretchy_buffer (len == sbcount(attr_sizes))
    int* attr_sizes; // stretchy_buffer
    char** attr_names; // stretchy_buffer

    // TODO set per attr
    bool instance_dirty;
};
struct l3d_texture {
    GLuint ptr;
    int w, h;
};
struct l3d_fbo {
    struct l3d_texture tex;
    struct l3d_texture depth_tex;
    GLuint fbo;
    float r,g,b,a;
};

struct i_l3d_mesh_bank_entry {
    l3d_ident model_name;
    struct l3d_mesh* mesh;
};
struct l3d_mesh_bank {
    struct i_l3d_mesh_bank_entry* entries; // stretchy_buffer
};

struct l3d_context {
    struct l3d_fbo main_framebuffer;
};

extern struct l3d_context* l3d_context__;

void
i_l3d_view_pv(struct l3d_view*, mat4x4 out);

void
i_calc_tangents(struct l3d_vertex* vs, int count);

void
i_mesh_calculate_bsphere(struct i_sphere*, struct l3d_vertex*, uint32_t count);

void
i_debug_mat4(mat4x4);

#endif
