#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#ifdef _WIN32
#define l3d_PATH_SEP '\\'
#else
#define l3d_PATH_SEP '/'
#endif

static
void
write_u8(FILE* f, uint8_t v) {
    uint8_t b[1];
    b[0] = v;
    fwrite(b, 1, 1, f);
}

static
void
write_u16(FILE* f, uint32_t v) {
    uint8_t b[2];
    b[0] = (v >> 0) & 0xff;
    b[1] = (v >> 8) & 0xff;
    fwrite(b, 2, 1, f);
}

static
void
write_u32(FILE* f, uint32_t v) {
    uint8_t b[4];
    b[0] = (v >> 0) & 0xff;
    b[1] = (v >> 8) & 0xff;
    b[2] = (v >> 16) & 0xff;
    b[3] = (v >> 24) & 0xff;
    fwrite(b, 4, 1, f);
}

static
void
write_f32(FILE* f, float v) {
    write_u32(f, *((uint32_t*)&v));
}

static
void
write_string(FILE* f, const struct aiString* v) {
    write_u32(f, v->length);
    fwrite(v->data, v->length, 1, f);
}

static
void
pack_recursive(const struct aiScene* scene, const struct aiNode* nd,
        FILE* write_ptr) {
    write_u8(write_ptr, nd->mNumMeshes);
    if (nd->mNumMeshes) {
        for (int n=0; n<nd->mNumMeshes; n++) {
            const struct aiMesh* mesh = scene->mMeshes[nd->mMeshes[n]];
            printf("    Mesh: %s  (verts: %d   faces: %d  bones: %d)\n", nd->mName.data, mesh->mNumVertices, mesh->mNumFaces, mesh->mNumBones);

            if (mesh->mNumVertices >= (1<<16)) {
                printf("Too many verts!\n");
                assert(0);
                return;
            }
            write_u16(write_ptr, mesh->mNumVertices);

            if (mesh->mNumFaces >= (1<<16)) {
                printf("Too many faces!\n");
                assert(0);
                return;
            }
            write_u16(write_ptr, mesh->mNumFaces);

            if (mesh->mNumBones >= (1<<16)) {
                printf("Too many bones!\n");
                assert(0);
                return;
            }
            write_u16(write_ptr, mesh->mNumBones);


            float* weights = calloc(mesh->mNumVertices*4, sizeof(float));
            uint16_t* ids = calloc(mesh->mNumVertices*4, sizeof(uint16_t));
            for (int bone_id=0; bone_id<mesh->mNumBones; ++bone_id) {
                const struct aiBone* bone = mesh->mBones[bone_id];
                for (int i=0; i<bone->mNumWeights; i++) {
                    const struct aiVertexWeight* w = &bone->mWeights[i];
                    float* ws = &weights[w->mVertexId*4];
                    float c = w->mWeight;
                    int smallest_i = -1;
                    float smallest = 10000000000;
                    for (int j=0; j<4; j++)
                        if (ws[j] < smallest) smallest = ws[j], smallest_i = j;
                    if (c > smallest) {
                        if (smallest_i == -1) {
                            printf("Error applying bone weights.\n");
                            assert(0);
                            return;
                        }
                        if (smallest > 0) {
                            //printf("WARNING: Vertex weighted to more than 4 bones. Ignoring smaller weight: %f\n", smallest);
                        }
                        ws[smallest_i] = c;
                        ids[w->mVertexId*4 + smallest_i] = bone_id;
                    } else {
                        //printf("WARNING: Vertex weighted to more than 4 bones. Ignoring smaller weight: %f\n", c);
                    }
                }
            }

            for (int i=0; i<mesh->mNumVertices; i++) {
                write_f32(write_ptr, mesh->mVertices[i].x);
                write_f32(write_ptr, mesh->mVertices[i].y);
                write_f32(write_ptr, mesh->mVertices[i].z);
                write_f32(write_ptr, mesh->mNormals[i].x);
                write_f32(write_ptr, mesh->mNormals[i].y);
                write_f32(write_ptr, mesh->mNormals[i].z);
                write_f32(write_ptr, mesh->mTangents[i].x);
                write_f32(write_ptr, mesh->mTangents[i].y);
                write_f32(write_ptr, mesh->mTangents[i].z);
                if (mesh->mTextureCoords[0]) {
                    write_f32(write_ptr, mesh->mTextureCoords[0][i].x);
                    write_f32(write_ptr, 1.0 - mesh->mTextureCoords[0][i].y);
                } else {
                    write_f32(write_ptr, 0);
                    write_f32(write_ptr, 0);
                }

                for (int j=0; j<4; j++) {
                    write_f32(write_ptr, weights[i*4 + j]);
                }
                for (int j=0; j<4; j++) {
                    write_u16(write_ptr, ids[i*4 + j]);
                }
            }

            free(weights);
            free(ids);

            for (int t = 0; t<mesh->mNumFaces; ++t) {
                const struct aiFace* face = &mesh->mFaces[t];
                if (face->mNumIndices != 3) {
                    printf("\nMesh face has wrong number of indices!\n");
                    assert(0);
                    return;
                }
                write_u16(write_ptr, face->mIndices[0]);
                write_u16(write_ptr, face->mIndices[1]);
                write_u16(write_ptr, face->mIndices[2]);
            }

            for (int t = 0; t<mesh->mNumBones; ++t) {
                const struct aiBone* bone = mesh->mBones[t];
                write_string(write_ptr, &bone->mName);
            }

            for (int t = 0; t<mesh->mNumBones; ++t) {
                // Assimp uses row-major. We convert to column major here.
                const struct aiBone* bone = mesh->mBones[t];

                write_f32(write_ptr, bone->mOffsetMatrix.a1);
                write_f32(write_ptr, bone->mOffsetMatrix.b1);
                write_f32(write_ptr, bone->mOffsetMatrix.c1);
                write_f32(write_ptr, bone->mOffsetMatrix.d1);

                write_f32(write_ptr, bone->mOffsetMatrix.a2);
                write_f32(write_ptr, bone->mOffsetMatrix.b2);
                write_f32(write_ptr, bone->mOffsetMatrix.c2);
                write_f32(write_ptr, bone->mOffsetMatrix.d2);

                write_f32(write_ptr, bone->mOffsetMatrix.a3);
                write_f32(write_ptr, bone->mOffsetMatrix.b3);
                write_f32(write_ptr, bone->mOffsetMatrix.c3);
                write_f32(write_ptr, bone->mOffsetMatrix.d3);

                write_f32(write_ptr, bone->mOffsetMatrix.a4);
                write_f32(write_ptr, bone->mOffsetMatrix.b4);
                write_f32(write_ptr, bone->mOffsetMatrix.c4);
                write_f32(write_ptr, bone->mOffsetMatrix.d4);
            }
        }
    }

    write_u8(write_ptr, nd->mNumChildren);
    for (int n=0; n<nd->mNumChildren; n++) {
		pack_recursive(scene, nd->mChildren[n], write_ptr);
	}
}

static
int
loadasset(const char* path, FILE* write_ptr) {
	const struct aiScene* scene = aiImportFile(path,
             aiProcess_Triangulate
            |aiProcess_GenUVCoords
            |aiProcess_GenSmoothNormals
            |aiProcess_SortByPType
            |aiProcess_RemoveRedundantMaterials
            |aiProcess_ImproveCacheLocality
            |aiProcess_CalcTangentSpace
            |aiProcess_PreTransformVertices
            |aiProcess_ValidateDataStructure
            |aiProcess_RemoveRedundantMaterials
            |aiProcess_FindInstances
            );

	if (!scene) return 1;
    printf("%s\n", path);
    pack_recursive(scene, scene->mRootNode, write_ptr);

    write_u32(write_ptr, scene->mNumAnimations);
    for (int i=0; i<scene->mNumAnimations; i++) {
        const struct aiAnimation* anim = scene->mAnimations[i];
        printf("    Anim: %s  (mesh channels: %d  channels: %d)\n",
                anim->mName.data,
                anim->mNumMeshChannels,
                anim->mNumChannels);
        for (int j=0; j<anim->mNumChannels; j++) {
            const struct aiNodeAnim* na = anim->mChannels[j];
            printf("        %s  (scale: %d  rot: %d  trans: %d)\n",
                    na->mNodeName.data,
                    na->mNumScalingKeys,
                    na->mNumRotationKeys,
                    na->mNumPositionKeys);
        }
    }

	return 0;
}

int
main(int argc, char** argv) {
    if (argc < 3) {
        printf("Usage: lib3d_importer output_file file...\n");
        return 0;
    }

    FILE* write_ptr = fopen(argv[1], "wb");
    if (!write_ptr) {
        printf("ERROR: Failed to open \"%s\" for writing.\n", argv[1]);
        return 1;
    }

    write_u32(write_ptr, argc-2);
    printf("Packaging %d assets into %s\n", argc-2, argv[1]);
    for (int i=2; i<argc; i++) {
        const char* n = argv[i];
        int last_sep = 0;
        for (int j=0; j<strlen(n); j++) {
            if (n[j] == l3d_PATH_SEP)
                last_sep = j+1;
        }
        int len = 0;
        for (int j=last_sep; j<strlen(n); j++) {
            if (n[j] == '.')
                len = j-last_sep;
        }

        write_u32(write_ptr, len);
        fwrite(n+last_sep, len, 1, write_ptr);

        if (loadasset(argv[i], write_ptr) != 0) {
            printf("\nFailed to import %s\n", argv[i]);
        }
    }
    printf("\n");
    fclose(write_ptr);
}
