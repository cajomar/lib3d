#ifndef __LIB3D_H__
#define __LIB3D_H__
#include "lib3d_mat.h"
#include <stdint.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif


#ifndef L3D_EXPORTED
#if defined _WIN32
    #define L3D_EXPORTED __declspec(dllexport)
#else
    #define L3D_EXPORTED __attribute__((visibility("default")))
#endif
#endif

struct l3d_scene;
struct l3d_view;

struct l3d_fbo;
struct l3d_mesh;
struct l3d_mesh_bank;
struct l3d_model;
struct l3d_shader;
struct l3d_texture;
struct l3d_vertex;
struct i_sphere;

static const uint32_t l3d_FBO_DEPTH = 1<<0;
static const uint32_t l3d_FBO_HDR16 = 1<<1;
static const uint32_t l3d_FBO_HDR32 = 1<<2;
static const uint32_t l3d_FBO_R = 1<<3;
       
static const uint32_t l3d_TEXTURE_MIPMAP     = 1<<0;
static const uint32_t l3d_TEXTURE_CLAMP_X    = 1<<1;
static const uint32_t l3d_TEXTURE_CLAMP_Y    = 1<<2;
static const uint32_t l3d_TEXTURE_ANISOTROPY = 1<<3;
       
static const uint32_t l3d_MESH_CALCULATE_TANGENTS = 1<<0;


L3D_EXPORTED
int
l3d_init(void);

L3D_EXPORTED
void
l3d_viewport(int w, int h);

L3D_EXPORTED
int
l3d_shutdown(void);

L3D_EXPORTED
int
l3d_set_shaders_path(const char* path_prefix, const char* path_suffix);

L3D_EXPORTED
void
l3d_clear_shader_cache(void);

L3D_EXPORTED
int
l3d_add_shader_directive(const char* token, const char* directive);



/////////////////
// Identifiers //
/////////////////

typedef uint64_t l3d_ident;

L3D_EXPORTED
l3d_ident
l3d_ident_from_str(const char*);

L3D_EXPORTED
l3d_ident
l3d_ident_from_strn(const char* str, int len);

L3D_EXPORTED
const char*
l3d_ident_as_char(l3d_ident);



///////////
// Scene //
///////////

// An OpenGL context should be created before calling l3d_scene_new.
L3D_EXPORTED
struct l3d_scene*
l3d_scene_new(void);

// Will detach any models. Will NOT free them.
L3D_EXPORTED
void
l3d_scene_delete(struct l3d_scene*);

L3D_EXPORTED
struct l3d_view*
l3d_scene_view(struct l3d_scene*);

L3D_EXPORTED
void
l3d_scene_set_view(struct l3d_scene*, struct l3d_view*);

// Set `target` to NULL to render to main framebuffer.
struct l3d_scene_render_conf {
    struct l3d_fbo* target;
    uint64_t time_past;
    int pass;
    bool clear;
    bool blend;
    bool depth_test;
    bool depth_writable;
    bool cull_face;
};

L3D_EXPORTED
void
l3d_scene_render(struct l3d_scene*, struct l3d_scene_render_conf*);

L3D_EXPORTED
int
l3d_scene_add_model(struct l3d_scene*, struct l3d_model*);

L3D_EXPORTED
int
l3d_scene_remove_model(struct l3d_scene*, struct l3d_model*);


struct l3d_intersection {
    struct l3d_model* model;
    struct l3d_mesh* mesh;
    vec3 position;
    vec3 triangle_bary;
    uint16_t face_index;
};

L3D_EXPORTED
struct l3d_intersection*
l3d_scene_intersect_screen(struct l3d_scene*, int x, int y, int* count);

L3D_EXPORTED
void
l3d_release_intersections(struct l3d_intersection*);


//////////////
// Textures //
//////////////

L3D_EXPORTED
struct l3d_texture*
l3d_texture_from_path(const char*, uint32_t texture_flags);

L3D_EXPORTED
struct l3d_texture*
l3d_texture_from_data(uint8_t* data, int w, int h, int bpp, uint32_t flags);

L3D_EXPORTED
void
l3d_texture_delete(struct l3d_texture*);

L3D_EXPORTED
int
l3d_texture_width(struct l3d_texture*);

L3D_EXPORTED
int
l3d_texture_height(struct l3d_texture*);



/////////
// FBO //
/////////

L3D_EXPORTED
struct l3d_fbo*
l3d_fbo_new(int width, int height, uint32_t fbo_flags, uint32_t texture_flags);

L3D_EXPORTED
void
l3d_fbo_delete(struct l3d_fbo*);

L3D_EXPORTED
struct l3d_texture*
l3d_fbo_reader(struct l3d_fbo*);

L3D_EXPORTED
struct l3d_texture*
l3d_fbo_depth_reader(struct l3d_fbo*);

L3D_EXPORTED
void
l3d_fbo_set_clear_color(struct l3d_fbo*, float r, float g, float b, float a);



/////////////
// Shaders //
/////////////

struct l3d_shader_config {
    // Texture slots must be tightly packed. Any keys after a NULL slot are ignored.
    const char* texture_slots[32];
};

L3D_EXPORTED
struct l3d_shader*
l3d_shader_load(const char* key, const char* variant, struct l3d_shader_config const*);

L3D_EXPORTED
void
l3d_shader_set_uniform(struct l3d_shader*, const char* key, int comps, float* value);



///////////
// Model //
///////////

L3D_EXPORTED
struct l3d_model*
l3d_model_new(void);

L3D_EXPORTED
void
l3d_model_delete(struct l3d_model*);

L3D_EXPORTED
void
l3d_model_set_transform(struct l3d_model*, mat4x4 T);

L3D_EXPORTED
void
l3d_model_set_mesh(struct l3d_model*, int mesh_slot, struct l3d_mesh*);

L3D_EXPORTED
void
l3d_model_remove_mesh(struct l3d_model*, int mesh_slot);

L3D_EXPORTED
void
l3d_model_set_mesh_transform(struct l3d_model*, int mesh_slot, mat4x4 T);

L3D_EXPORTED
void
l3d_model_get_bounding_sphere(struct l3d_model*, struct i_sphere* out);

L3D_EXPORTED
void
l3d_model_set_texture(struct l3d_model*, int mesh_slot, int texture_slot, struct l3d_texture*);

// Pass -1 to `mesh_slot` to apply to all meshes.
// NULL can be passed to `shader` to skip drawing on the specified `pass`
L3D_EXPORTED
void
l3d_model_set_shader(struct l3d_model*, struct l3d_shader* shader, int mesh_slot, int pass);

L3D_EXPORTED
void
l3d_model_set_tess_level(struct l3d_model*, float tess_level);



////////////////
// Instancing //
////////////////

L3D_EXPORTED
int
l3d_model_instance(struct l3d_model*);

L3D_EXPORTED
int
l3d_model_new_instance_attr(struct l3d_model*, const char* name, int component_count);

L3D_EXPORTED
void
l3d_instance_attr(struct l3d_model*, int instance_id, int attr_id, float*);



//////////
// Geom //
//////////

L3D_EXPORTED
struct l3d_mesh*
l3d_geom_plane(float sx, float sy);

L3D_EXPORTED
struct l3d_mesh*
l3d_geom_cube(float sx, float sy, float sz, vec2 uv[4]);

// writes 6 vertices to `target` to create a square.
// Seem will run between `top_left` and `bottom_right`.
L3D_EXPORTED
void
l3d_geom_write_square(struct l3d_vertex* target,
        vec3 top_left,
        vec3 top_right,
        vec3 bottom_right,
        vec3 bottom_left,
        vec2 uv[4]);



//////////
// Mesh //
//////////

struct l3d_vertex {
    vec3 position;
    vec3 normal;
    vec3 tangent_normal;
    float u, v;
    float bone_weights[4];
    uint16_t bone_ids[4];
};
struct i_sphere {
    vec3 center;
    float rad;
};
struct l3d_mesh {
    unsigned int vbo, index_vbo;
    uint32_t index_count;
    struct l3d_vertex* verts;
    unsigned short* indicies;
    unsigned short vertex_count;
    unsigned short face_count;
    struct i_sphere bsphere;
    float* bones; // stretchy_buffer
    l3d_ident* bone_names; // stretchy_buffer
    bool indexed;
};

L3D_EXPORTED
struct l3d_mesh_bank*
l3d_mesh_bank_load(const char* path);

L3D_EXPORTED
struct l3d_model*
l3d_mesh_bank_get(struct l3d_mesh_bank*, l3d_ident model_name);

L3D_EXPORTED
struct l3d_mesh*
l3d_mesh_new(struct l3d_vertex* vertices, uint32_t vertex_count, uint32_t mesh_flags);



//////////
// View //
//////////

/* Common uses:
 * mat4x4_perspective(view->projection, fov, aspect_ratio, near, far);
 */
struct l3d_view {
    vec3 translate;
    vec3 look_at;
    mat4x4 projection;
};

#ifdef __cplusplus
}
#endif

#endif
